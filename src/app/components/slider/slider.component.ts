import { AfterViewInit, Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'slider-images',
	templateUrl: './slider.component.html',
	styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit, AfterViewInit {

	@Input() imageUrl: Array<string> = [];
	slideIndex = 1;

	constructor() { }

	ngOnInit(): void {}

	ngAfterViewInit() {
		this.showSlides(this.slideIndex);
	}

	plusSlides(slidesChange: number) {
		this.showSlides(this.slideIndex += slidesChange);
	}

	showSlides(slideIndex: number) {
		let slides: any = document.getElementsByClassName("mySlides");
		let dots = document.getElementsByClassName("dot");
		if (slideIndex > slides.length) { this.slideIndex = 1 }
		if (slideIndex < 1) { this.slideIndex = slides.length }
		for (let i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (let i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace(" active", "");
		}
		slides[this.slideIndex - 1].style.display = "block";
	}

	currentSlide(actualSlideIndex: number) {
		this.showSlides(this.slideIndex = actualSlideIndex);
	}

}
