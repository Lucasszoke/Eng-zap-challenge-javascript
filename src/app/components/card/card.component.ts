import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'immobile-card',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

	@Input() imageUrl: [string];
	@Input() rent: string;
	@Input() description: string;
	@Input() immobileData: any;

	constructor() {}

	ngOnInit() {}

}
