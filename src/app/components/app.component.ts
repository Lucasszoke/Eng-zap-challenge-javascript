import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	private readonly urlService = 'https://run.mocky.io/v3/c09deeab-e7ed-4b55-bfd7-013ba2a3a31f';
	image = 'https://resizedimgs.zapimoveis.com.br/crop/420x236/vr.images.sp/a0fb8295f627596ba1b113f5b65f2a96.jpg';
	imageUrl: Array<string> = [this.image, this.image, this.image, this.image, this.image];
	rent: string = 'R$ 3.000';
	description: string = 'Linda casa, espaçosa, em ótimas condições, com armários embutidos, salão de festa com churrasqueira coberto, lavanderia grande e vizinhança agradável.';
	immobileData: any = { addressName: 'Rua Antônio Gil, Cidade Ademar', usableArea: '170', rooms: '2', garages: '3', bathrooms: '2' };

	constructor(private http: HttpClient) {
		this.getImmobiles();
	}

	getImmobiles() {
		this.http.get(this.urlService)
			.subscribe((data) => {
				console.log(data)
			}, (err) => {
				console.log(err);
			});
	}


}
