# EngZapChallengeJavascript

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.0.

## Como executar o projeto

1) Para executar o projeto é necessário baixá-lo com o seguinte comando... (a criar pasta do github)
2) Após feito o download do projeto execute dentro da pasta raíz o comando "npm install"
3) Feito o Download das bibliotecas do projeto, abra o site "https://designer.mocky.io/", ele será usado para "mockar" a chamada Http que contém os dados disponíveis dentro do arquivo Json (http://grupozap-code-challenge.s3-website-us-east-1.amazonaws.com/sources/source-1.json).
3.1) Para mockar a chamada HTTP basta clicar em um botão escrito "New Mock" no canto superior direito do site, definir o status da resposta como 200 e definir o corpo da resposta como os dados informados no arquivo Json (http://grupozap-code-challenge.s3-website-us-east-1.amazonaws.com/sources/source-1.json).
3.2) Criado o mock do serviço, dentro do componente "app.component.ts" existe uma variável chamada "urlService", altere o valor desta variável para ser igual ao valor gerado pelo site.
4) Após feitos esses passos execute o comando "npm start", será iniciado o projeto dentro da porta 4200, acesse o site através do link "http://localhost:4200/"
